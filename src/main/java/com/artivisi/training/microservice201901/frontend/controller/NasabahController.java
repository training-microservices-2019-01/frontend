package com.artivisi.training.microservice201901.frontend.controller;

import com.artivisi.training.microservice201901.frontend.dto.Nasabah;
import com.artivisi.training.microservice201901.frontend.service.NasabahClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class NasabahController {

    private static final Logger LOGGER = LoggerFactory.getLogger(NasabahController.class);

    @Autowired private NasabahClient nasabahClient;

    @GetMapping("/nasabah/list")
    public ModelMap dataNasabah() {
        LOGGER.info("Mengakses data nasabah");
        return new ModelMap()
                .addAttribute("dataNasabah", nasabahClient.semuaNasabah());
    }

    @GetMapping("/nasabah/form")
    public void tampilkanForm(){}


    @PostMapping("/nasabah/form")
    public String prosesForm(@ModelAttribute Nasabah nasabah) {
        LOGGER.info("Simpan data nasabah : " + nasabah);
        nasabahClient.simpan(nasabah);
        return "redirect:list";
    }
}
