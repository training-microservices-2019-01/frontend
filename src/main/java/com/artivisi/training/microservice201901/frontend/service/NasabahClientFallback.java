package com.artivisi.training.microservice201901.frontend.service;

import com.artivisi.training.microservice201901.frontend.dto.Nasabah;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class NasabahClientFallback implements NasabahClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(NasabahClientFallback.class);

    @Override
    public Iterable<Nasabah> semuaNasabah() {
        LOGGER.info("Menjalankan fallback method semuaNasabah. Return List kosong");
        return new ArrayList<>();
    }

    @Override
    public void simpan(Nasabah nasabah) {
        LOGGER.info("Backend sedang offline, data tidak tersimpan");
    }
}
