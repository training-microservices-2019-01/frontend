package com.artivisi.training.microservice201901.frontend.service;

import com.artivisi.training.microservice201901.frontend.dto.Nasabah;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "nasabah", fallback = NasabahClientFallback.class)
public interface NasabahClient {

    @GetMapping("/nasabah/semua")
    Iterable<Nasabah> semuaNasabah();

    @PostMapping("/nasabah/")
    void simpan(@RequestBody Nasabah nasabah);
}
